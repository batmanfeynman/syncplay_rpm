# Syncplay
This repository contains a spec file to build [syncplay](https://github.com/Syncplay/syncplay) for fedora!

# Instructions
The instructions are adapted from [here](https://docs.fedoraproject.org/en-US/package-maintainers/Package_Maintenance_Guide/#using_fedpkg_anonymously)
```
git clone #this repository
cd #repository directory
spectool -g syncplay.spec
```
